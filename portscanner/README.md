# Portscanner - A TCP Port Scanning Tool
Wenjing Wu

## What Was Built
Portscanner reproduces some functionalities for TCP port scan of [nmap](https://nmap.org/book/reduce-scantime.html).
Concurrent scanning was implemented and can be ajust using `-b/--batch_size` flag. 
In addition, colored output and progress bar were added For better user expericece. 

Besides open ports, nmap also returns the defaut service types recommended by most systems. 
I believe the name of service types and their associated port numbers are prestored somewhere in nmap database. 
Instead of the default service type, portscanner grabs the service information from TCP socket reply. 
In order to do so, portscanner has to send a "hello" message to each open port through the connection stream and parse the response. 

## How it works


Usage and command-line flags are listed below:

```shell
port scanner 0.1.0

USAGE:
    portscanner [OPTIONS] <host>

ARGS:
    <host>    The host address to scan

OPTIONS:
    -b, --batch_size <batch_size>      Number of ports that will be scanned concrrently. Could cause
                                       problems if it is too big. Reduce batch_size and try again if
                                       scan returns zero open port  [default: 1000]
    -e, --range_end <range_end>        End of port range
    -f, --full_range                   Scan port 0-65535
    -h, --help                         Print help information
    -p, --port <port>                  Scan single port
    -s, --range_start <range_start>    Start of port range
    -t, --timeout <timeout>            Connection timeout in milliseconds  [default: 1000]
    -V, --version                      Print version information
```

To get the help information above: `cargo run` or `Cargo run -- -h/--help`

Example: 
* Scan by range: `cargo run -- kerkour.com -s 80 -e 500`
* Scan by single port: `cargo run -- kerkour.com -p 8080 `
* Scan by top 1000 most commonly used tcp ports: `cargo run -- kerkour.com `
* Scan all ports: `cargo run -- kerkour.com -f`
* Scan with a batch_size flag(default 1000): `cargo run -- kerkour.com -f -b 200`
* Scan with a timeout flag(default 1000 mili): `cargo run -- kerkour.com -s 8000 -e 9000 -t 500`


## What Didn't Work
* Scanning hangs or returns empty service sometimes. The problem can be fixed by reset batch_size or port range.
I suspect it's either caused by concrrency or the target server is doing something to prevent malicious scan. I need to investigate it further and improve error handling.
* I didn't end up having time to implement UDP port scanning. But it was a stretch goal anyhow.


## What Lessons Were Learned
* I had fun of playing with closures, futures, async functions and concurrency thoughout this project. 
* `StreamExt::for_each_concurrent` only takes function that returns unit type. I misread this part in the documentaiton and wasted a lot of time on it. 



## Refernces
* https://stackoverflow.com/questions/51044467/how-can-i-perform-parallel-asynchronous-http-get-requests-with-reqwest
* https://kerkour.com/rust-worker-pool/
* https://github.com/console-rs/indicatif
* https://github.com/RustScan/RustScan/tree/master/src
* https://kerkour.com/rust-fast-port-scanner
