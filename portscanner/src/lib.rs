//! Portscanner - A TCP Port Scanning Tool!
//!
//! Wenjing Wu - Winter 2022

//references:
//https://stackoverflow.com/questions/51044467/how-can-i-perform-parallel-asynchronous-http-get-requests-with-reqwest
//https://github.com/RustScan/RustScan/tree/master/src

use async_std::io;
use async_std::io::{ReadExt, WriteExt};
use async_std::net::TcpStream;

use futures::{stream, StreamExt};
use std::io::ErrorKind;

use std::net::{IpAddr, Shutdown, SocketAddr};
use std::str::from_utf8;

use indicatif::{ProgressBar, ProgressStyle};
use std::time::{Duration, Instant};

mod nmap_top_tcp_ports;

pub struct Scanner {
    timeout: Duration,
}

impl Scanner {
    pub fn new(timeout: Duration) -> Self {
        Self { timeout }
    }
    pub async fn scan(
        &self,
        host: IpAddr,
        start_port: Option<u16>,
        end_port: Option<u16>,
        batch_size: u16,
        full: bool,
        single_port: Option<u16>,
    ) -> Vec<(SocketAddr, String)> {
        run(
            host,
            start_port,
            end_port,
            batch_size,
            self.timeout,
            full,
            single_port,
        )
        .await
    }
}

///Try connect all ports concurrently
///Function returns a vector of tuples (ip:port, serive) that represents open addresss and their serive types.
async fn run(
    host: IpAddr,
    start_port: Option<u16>,
    end_port: Option<u16>,
    batch_size: u16,
    timeout: Duration,
    full: bool,
    single_port: Option<u16>,
) -> Vec<(SocketAddr, String)> {
    let instant = Instant::now(); //start time recording

    let mut all_ports = Vec::new();
    if single_port.is_some() {
        //single port scan
        let single = single_port.unwrap();
        all_ports.push(single);
    } else if !full && start_port.is_none() && end_port.is_none() {
        //top 1000 most common tcp ports scan
        all_ports = nmap_top_tcp_ports::MOST_COMMON_TCP_PORTS.to_vec();
    } else {
        //scan based on range, including full scan
        let start = start_port.unwrap();
        let end = end_port.unwrap();
        all_ports = (start..=end).collect();
    }

    //create and set a progress bar
    let pb = ProgressBar::new(all_ports.len().try_into().unwrap());
    pb.set_style(
        ProgressStyle::with_template(
            "{spinner:.green} [{elapsed_precise}] [{bar:40.cyan/blue}] {pos}/{len} {msg}",
        )
        .unwrap()
        .with_key("eta", |state| format!("{:.1}s", state.eta().as_secs_f64())), //.progress_chars("#>-")
    );

    let results: Vec<Result<(SocketAddr, String), _>> = stream::iter(all_ports)
        .enumerate()
        .map(|(i, port)| {
            //set progress bar position and display port number at the right end of bar
            pb.set_position(i.try_into().unwrap());
            pb.set_message(format!("port {}", port));

            async move {
                match try_connect(host, &port, timeout).await {
                    Ok(tup) => Ok(tup), //(open port, serive)
                    Err(e) => Err(e),
                }
            }
        })
        .buffer_unordered(batch_size.into())
        .collect()
        .await;

    //convert the success results into vector
    let open_addrs: Vec<(SocketAddr, String)> =
        results.into_iter().filter_map(|r| r.ok()).collect();

    //clear porgress bar
    pb.finish_and_clear();
    //print time elapsed information
    println!(
        "Port scan finished in { } seconds.",
        instant.elapsed().as_secs()
    );

    open_addrs
}

///Try tcp connect to a remote address(ip:port). A successful connection indicates the given port is open for the ip.
///If the port is open, send 'hi' into the stream to get the service type from reply.
///Funtion returns open address(ip:port) and corresponding serive type as tuple, if there is no error.
async fn try_connect(
    host: IpAddr,
    port: &u16,
    timeout: Duration,
) -> io::Result<(SocketAddr, String)> {
    //println!("Trying port {}", addr.port());
    let addr = SocketAddr::new(host, *port);
    match connect(addr, timeout).await {
        Ok(stream_result) => match get_service(stream_result).await {
            Ok(service) => Ok((addr, service)),
            Err(e) => {
                eprintln!("3 {}", e);
                Err(e)
            }
        },
        Err(e) => match e.kind() {
            ErrorKind::Other => {
                eprintln!("4 {}", e);
                Err(e)
            }
            _ => Err(io::Error::new(io::ErrorKind::Other, e.to_string())),
        },
    }
}

///Opens a TCP connection to a remote address(ip:port)
///If connection succeeds before timeout(meaning port is open), return tcp socket stream.
async fn connect(addr: SocketAddr, timeout: Duration) -> io::Result<TcpStream> {
    let stream = io::timeout(timeout, async move { TcpStream::connect(addr).await }).await?;
    Ok(stream)
}

///Write a 'hi' message into the sream and read the reply.
///Parse the serive type from the reply.
///Function returns servce type if no error.
async fn get_service(mut stream: TcpStream) -> io::Result<String> {
    let msg = b"Hi!";
    let reply = match stream.write(msg).await {
        Ok(_) => {
            let mut data = [0_u8; 64]; //buffer size 64 bytes

            //println!("here1");
            match stream.read(&mut data).await {
                Ok(_) => {
                    //println!("here2");
                    let mut text = from_utf8(&data) //convert read bytes to utf8 message
                        .unwrap()
                        .splitn(2, |c| c == ' ' || c == '\r' || c == '\n'); //split message into two parts using space or line break
                    let first = text.next().unwrap(); // the first part of split contains service name
                    Ok(first.to_string()) // return service name as string
                }
                Err(_) => {
                    //stream.shutdown(Shutdown::Write).expect("could not shutdown");
                    //println!("Failed to read");
                    Ok("Fail to read".to_string())
                }
            }
        }
        Err(_) => {
            /* stream
            .shutdown(Shutdown::Write)
            .expect("could not shutdown"); */
            //println!("Failed to write");
            Ok("Fail to write".to_string())
        }
    };

    match reply {
        Ok(service) => match stream.shutdown(Shutdown::Both) {
            Ok(_) => Ok(service),
            Err(e) => {
                eprintln!("1 {}", e);
                Err(e)
            }
        },
        Err(e) => {
            eprintln!("2 {}", e);
            Err(e)
        }
    }
}

#[cfg(test)]
mod tests {
    //use super::Scanner;
    use crate::connect;
    use crate::get_service;
    //use crate::try_connect;
    use async_std::net::{TcpListener, TcpStream};
    use async_std::task;
    use std::net::SocketAddr;
    use std::time::Duration;

    #[tokio::test]
    async fn test_get_service_ssh() {
        //github.com:22
        let stream = TcpStream::connect("192.30.255.112:22").await.unwrap();
        let result = get_service(stream).await.unwrap();
        assert!(result.starts_with("SSH"));
    }
    #[tokio::test]
    async fn test_get_service_http() {
        //kerkour.com:80
        let stream = TcpStream::connect("172.66.43.21:80").await.unwrap();
        let result = get_service(stream).await.unwrap();
        assert!(result.starts_with("HTTP"));
    }

    #[tokio::test]
    async fn test_connect() {
        let sock_addr1 = SocketAddr::from(([127, 0, 0, 1], 1025));
        let listener1 = TcpListener::bind(sock_addr1).await.unwrap();
        task::spawn(async move { listener1.accept().await });
        let conn = connect(sock_addr1, Duration::from_secs(3)).await;
        assert!(conn.is_ok());
    }
}
