//! Portscanner - A TCP Port Scanning Tool!
//!
//! Wenjing Wu - Winter 2022

//references: https://github.com/console-rs/indicatif

use async_std::task;
use clap::{Arg, Command};
use colored::Colorize;
use portscanner::Scanner;
use std::error::Error;
use std::net::{SocketAddr, ToSocketAddrs};
use std::time::Duration;

fn main() -> Result<(), Box<dyn Error>> {
    let cli = Command::new("port scanner")
        .version("0.1.0")
        .arg_required_else_help(true)
        .arg(
            Arg::new("host")
                .help("The host address to scan")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::new("timeout")
                .help("Connection timeout in milliseconds ")
                .long("timeout")
                .short('t')
                .takes_value(true)
                .default_value("1000"),
        )
        .arg(
            Arg::new("batch_size")
                .help("Number of ports that will be scanned concrrently. Could cause problems if it is too big. Reduce batch_size and try again if scan returns zero open port ")
                .long("batch_size")
                .short('b')
                .takes_value(true)
                .default_value("1000"),
        )
        .arg(
            Arg::new("range_start")
                .help("Start of port range")
                .long("range_start")
                .short('s')
                .requires("range_end")
                .takes_value(true),
        )
        .arg(
            Arg::new("range_end")
                .help("End of port range ")
                .long("range_end")
                .requires("range_start")
                .short('e')
                .takes_value(true),
        )
        .arg(
            Arg::new("full_range")
                .help("Scan port 0-65535 ")
                .long("full_range")
                .short('f'),
        )
        .arg(
            Arg::new("port")
                .help("Scan single port")
                .long("port")
                .short('p')
                .takes_value(true),
        )
        .get_matches();
    let host = cli.value_of("host").unwrap();

    let timeout = cli
        .value_of("timeout")
        .unwrap()
        .parse::<u64>()
        .unwrap_or(1000);
    let batch_size = cli
        .value_of("batch_size")
        .unwrap()
        .parse::<u16>()
        .unwrap_or(1000);

    let ip_addresses: Vec<SocketAddr> = format!("{}:0", host).to_socket_addrs()?.collect();
    let ps = Scanner::new(Duration::from_millis(timeout));
    let results: Vec<(SocketAddr, String)>;

    //Display title, current configuration and help information of the tool
    let line = format!("{:-<150}", "-").cyan();
    println!("{ }", line);
    println!("{ }", "Portscanner - A TCP Port Scanning Tool".bold());
    println!("By Wenjing Wu");
    println!("{ }", line);
    println!(
        "Timeout(milliseconds): { }    Batch size: { }",
        timeout, batch_size
    );
    println!("For help, add flag -h/-help or simply use command 'cargo run'.");
    println!("{ }", line);

    if cli.is_present("port") {
        //single port scan
        let single_port = cli.value_of("port").unwrap().parse::<u16>().unwrap_or(0);
        println!("Scan single port for IP { }", ip_addresses[0].ip());
        let ftr = ps.scan(
            ip_addresses[0].ip(),
            None,
            None,
            batch_size,
            false,
            Some(single_port),
        );
        results = task::block_on(async { ftr.await });
    } else if !cli.is_present("range_start")
        && !cli.is_present("range_end")
        && !cli.is_present("full_range")
    {
        // top 1000 most commonly used ports scan
        println!(
            "Scan top 1000 most commonly used ports for IP { }",
            ip_addresses[0].ip()
        );
        let ftr = ps.scan(ip_addresses[0].ip(), None, None, batch_size, false, None);
        results = task::block_on(async { ftr.await });
    } else if cli.is_present("range_start") && cli.is_present("range_end") {
        //scan by range
        let range_start = cli
            .value_of("range_start")
            .unwrap()
            .parse::<u16>()
            .unwrap_or(0);
        let range_end = cli
            .value_of("range_end")
            .unwrap()
            .parse::<u16>()
            .unwrap_or(65535);
        println!(
            "Scan port from range { } to { } for IP { } ",
            range_start,
            range_end,
            ip_addresses[0].ip()
        );

        let ftr = ps.scan(
            ip_addresses[0].ip(),
            Some(range_start),
            Some(range_end),
            batch_size,
            false,
            None,
        );
        results = task::block_on(async { ftr.await });
    } else {
        //full scan
        println!("Scan all ports (0-65535) for IP { }", ip_addresses[0].ip());
        let ftr = ps.scan(
            ip_addresses[0].ip(),
            Some(0),
            Some(65535),
            batch_size,
            true,
            None,
        );
        results = task::block_on(async { ftr.await });
    }

    //print ip addresses converted from the host name but not scanned
    if ip_addresses.len() > 1 {
        let iter: String = ip_addresses
            .iter()
            .skip(1) // skip the scanned ip address
            .enumerate()
            .map(|(i, &addr)| match i {
                0 => (addr.ip().to_string() + " "),
                _ => match i % 6 {
                    0 => (addr.ip().to_string() + "\n"),
                    _ => (addr.ip().to_string() + ", "),
                },
            })
            .collect();
        println!("Other addresses for { } (not scanned):  { }", host, iter);
    }

    println!(" ");

    //print the scanned ip address (first address converted from host name)
    println!("IP:{ }", ip_addresses[0].ip());

    //print scan result
    if !results.is_empty() {
        println!(
            "{0: <10} {1: <5} {2: <15}",
            "Open Ports", "State", "Service(reply)"
        );
        for r in results {
            println!("{0: <10} {1: <5} {2: <15}", r.0.port(), "open", r.1);
        }
    } else {
        //suggest to reduce batch size if no open port found
        println!(
        "No open port found.\nCurrent batch size is { }.\nMaybe try again with a smaller batch size using flag '-b [batch size]'", batch_size);
    }
    Ok(())
}
