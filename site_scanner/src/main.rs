//! Tristan Gomez - Winter 2022
//!
//! Site Scanner, a website source code scanner.

use regex::Regex;
use reqwest::Url;
use std::fs::{File, OpenOptions};
use std::io::prelude::Write;
use std::{collections::HashMap, env, io::BufRead, io::BufReader};

#[tokio::main]
async fn main() {
    // Collect all command line arguments except for the first which is the program name.
    let args: Vec<String> = env::args().skip(1).collect();

    // Process the command-line arguments and put them into a HashMap
    let processed_args = process_arguments(&args);

    // If no individual url or wordlist file of urls is provided then give message, call usage() then exit.
    if !processed_args.contains_key("-u") && !processed_args.contains_key("-w") {
        eprintln!("No url(s) was/were provided.");
        usage();

    // If no pattern to look for is provided then scan cannot continue.
    // Give a message, call usage(), and exit.
    } else if !processed_args.contains_key("-p") && !processed_args.contains_key("-w") {
        eprintln!("No regular expression pattern was provided.");
        usage();
    }

    // If there is no provided wordlist, then look for the user supplied pattern.
    if !processed_args.contains_key("-w") {
        if let Ok(matches) = send_and_process_request(
            processed_args.get("-u").unwrap(),
            processed_args.get("-p").unwrap(),
        )
        .await
        {
            // Print the found matches.
            println!("Matches Found:\n");
            for (number, line) in matches.iter().enumerate() {
                println!("{} -> {}", number, line);
            }
        }

    // We have a wordlist file provided so we need to read in this list.
    } else {
        // Read in the wordlist and save the result.
        let generated_wordlist = generate_wordlist(processed_args.get("-w").unwrap());
        let mut matches: Vec<String> = Vec::new();
        let url = processed_args.get("-u").unwrap();
        match generated_wordlist {
            // Wordlist successfully read in
            Ok(words) => {
                // For each word/pattern in the wordlist, look for matches.
                for word in words {
                    // If matches are found then add them to 'matches'
                    if let Ok(mut page_match) = send_and_process_request(url, &word).await {
                        matches.append(&mut page_match);
                    }
                }

                // Print the matches found and number them.
                println!("Matches Found:\n");
                for (number, line) in matches.iter().enumerate() {
                    println!("{} -> {}", number, line);
                }

                // If an output file argument was provided then attempt to write the results to that file.
                if processed_args.contains_key("-o") {
                    match write_results_to_file(processed_args.get("-o").unwrap(), &matches) {
                        Ok(success_msg) => {
                            println!("{}", success_msg);
                        }
                        Err(err) => {
                            eprintln!("{}", err);
                        }
                    }
                }
            }
            Err(err) => {
                eprintln!("{}", err);
                usage();
            }
        }
    }

    std::process::exit(0);
}

/// This function takes two arguments. 'File_path' is a string slice and 'found' is a slice of String objects.
/// If the file already exists, then it is appended to. If the file doesn't exist, then a new file is created
/// and written to. This function returns a result containing a string.
pub fn write_results_to_file(file_path: &str, found: &[String]) -> Result<String, String> {
    // If the file exists, and is successfully opened.
    if let Ok(mut file) = OpenOptions::new().append(true).open(&file_path) {
        // For each result found, number it and write it to the file.
        for (num, result) in found.iter().enumerate() {
            let byte_str = format!("{} -> {}\n", num, result);
            match file.write(byte_str.as_bytes()) {
                // Successfully wrote line, continue as usual.
                Ok(_val) => continue,

                // Some write error occurred, stop writing to file and return error.
                Err(err) => {
                    return Err(err.to_string());
                }
            }
        }

        // Successfully wrote to file, return Ok
        let ret_val = format!("Successfully appended the scan results to {}", &file_path);
        Ok(ret_val)

    // File does not exist
    } else {
        // Attempt to create a new file at 'file_path'.
        match File::create(&file_path) {
            // File successfully created.
            Ok(mut file) => {
                // for each result found, number it and write it to file.
                for (num, regex_match) in found.iter().enumerate() {
                    let byte_str = format!("{} -> {}\n", num, regex_match);
                    match file.write(byte_str.as_bytes()) {
                        // successfully wrote line to file, continue as usual.
                        Ok(_val) => continue,

                        // Error when writing line, stop iterating through matches and return error.
                        Err(err) => return Err(err.to_string()),
                    }
                }
            }
            Err(_err) => {
                let ret_val = format!("Unable to create new file at {}", file_path);
                return Err(ret_val);
            }
        }
        let ok_msg = format!("Successfuly wrote results to {}", file_path);
        Ok(ok_msg)
    }
}

/// This function takes one argument as a string slice, which is an absolute file path.
/// This function returns a result. If Ok then return a vector of strings. If Err then
/// return a string.
///
/// This method reads the 'infile' line by line and populates a vector of strings with
/// the lines found.
pub fn generate_wordlist(infile: &str) -> Result<Vec<String>, String> {
    let mut list_to_search: Vec<String> = Vec::new();

    // attempt to open the file at 'infile'.
    let _file = match File::open(infile) {
        // File successfully opened.
        Ok(file_handle) => {
            // Read in all lines in the file.
            let lines = BufReader::new(file_handle).lines();

            // For each line, if Ok then add it to 'list_to_search'.
            // If err, then print an error message and call 'usage()'.
            for line in lines {
                if let Ok(word) = line {
                    list_to_search.push(word)
                } else {
                    eprintln!("Error when reading from file. Please check the contents/format of the provided wordlist.");
                    usage();
                }
            }

            return Ok(list_to_search);
        }
        Err(_err) => return Err("Unable to open file at provided path".to_string()),
    };
}

/// This async function handles the HTTP GET requests and searches the returned page for the 'pattern'.
/// It takes two arguments: 1) 'url' which is a string slice and, 2) 'pattern' which is a string slice.
/// 'url' is the page being requested and 'pattern' is the word or regex that is being searched.
/// This method returns result. Ok returns a vector of strings that are the found matches. The Err variant
/// returns a string indicating failure.
pub async fn send_and_process_request(url: &str, pattern: &str) -> Result<Vec<String>, String> {
    // Parse the url argument using reqwest::Url to put it into a form thatreqwest::get() can use.
    let parsed_url: Url = if !url.starts_with("http://") && !url.starts_with("https://") {
        let mut temp_url: String = "http://".to_string();
        temp_url += url;

        Url::parse(&temp_url).unwrap_or_else(|err| {
            eprintln!("{}", err);
            eprintln!(
                "An error occurred when parsing the provided url. Please check its accuracy."
            );
            usage();
        })
    } else {
        Url::parse(url).unwrap_or_else(|err| {
            eprintln!("{}", err);
            eprintln!(
                "An error occurred when parsing the provided url. Please check its accuracy."
            );
            usage();
        })
    };
    // Send the GET request and await the response.
    let site_text = reqwest::get(parsed_url).await;
    match site_text {
        // If we successfully receive a web page then we can process it.
        Ok(resp) => {
            // Convert the page's contents to utf-8.
            if let Ok(body) = resp.text_with_charset("utf-8").await {
                // Attempt to convert the provided pattern into a Regex object.
                let reg_exp = Regex::new(pattern).unwrap_or_else(|_| {
                    eprintln!(
                        "Error occurred when parsing provided pattern into regular expression."
                    );
                    usage();
                });

                let mut found_matches: Vec<String> = Vec::new();

                // Split the webpage into a vector of string slices by splitting at each newline character.
                let lines = body.split('\n').collect::<Vec<&str>>();

                // Check each line for a match. If a match is found then push the line to
                // the 'found_matches' vector.
                for line in lines {
                    if reg_exp.is_match(line) {
                        found_matches.push(line.to_string());
                    }
                }

                // If we have any match(es) then return them as Ok.
                if !found_matches.is_empty() {
                    Ok(found_matches)

                // If no matches are found return the proper error message.
                } else {
                    Err("Unable to find any regular expression matches.".to_string())
                }
            // There was some sort of error converting the page's contents to utf-8.
            // Print an error message and exit.
            } else {
                eprintln!("Failed to receive response text from web page request.");
                std::process::exit(1);
            }
        }

        // Never received a response or some sort of error in the response.
        // Print the error and call usage()
        Err(err) => {
            eprintln!("{}", err);
            usage();
        }
    }
}

/// This function takes a slice of an array of strings as its sole argument. The
/// argument contains the command-line parameters passed into the program. This function
/// processes the arguments and puts them into a HashMap that is then returned to 'main()'.
pub fn process_arguments(cmd_args: &[String]) -> HashMap<String, String> {
    let mut processed_args: HashMap<String, String> = HashMap::new();

    match cmd_args.len() {
        // no arguments provided, call usage() and exit.
        0 => usage(),
        1 => {
            // user passed in help flag, call display_man_page() and exit normally.
            if cmd_args[0] == "-h" || cmd_args[0] == "--help" {
                display_man_page();

            // user passed in 1 argument that wasn't the help flag. Invalid input,
            // call usage() and exit.
            } else {
                println!("Unrecognized command-line arguments.\n");
                usage();
            }
        }
        _ => {
            // Iterate through all provided command line arguments, process them, and insert them into
            // the HashMap. If an unrecognized argument is found then usage() is called and the program exits.
            let mut i = 0;
            while i < cmd_args.len() {
                if cmd_args[i] == "-p" || cmd_args[i] == "--pattern" {
                    processed_args.insert("-p".to_string(), cmd_args[i + 1].clone());
                    i += 2;
                } else if cmd_args[i] == "-u" || cmd_args[i] == "--url" {
                    processed_args.insert("-u".to_string(), cmd_args[i + 1].clone());
                    i += 2;
                } else if cmd_args[i] == "-w" || cmd_args[i] == "--wordlist" {
                    processed_args.insert("-w".to_string(), cmd_args[i + 1].clone());
                    i += 2;
                } else if cmd_args[i] == "-o" || cmd_args[i] == "--output" {
                    processed_args.insert("-o".to_string(), cmd_args[i + 1].clone());
                    i += 2;
                } else {
                    eprintln!("Unrecognized command-line argument.");
                    usage();
                }
            }

            processed_args
        }
    }
}

/// This function gives examples of proper usage for this program. It doesn't return and exits the
/// program with an error exit code.
pub fn usage() -> ! {
    println!("EXAMPLE: ./site_scanner -u <url> -p <pattern_to_search_for>");
    println!("EXAMPLE: ./site_scanner --url <url> --pattern <pattern_to_search_for");
    println!("EXAMPLE: ./site_scanner -w <absolute_path_to_file> -p <pattern_to_search_for>");
    println!("EXAMPLE: ./site_scanner --wordlist <absolute_path_to_file> --pattern <pattern_to_search_for>");
    println!("EXAMPLE: ./site_scanner -w <absolute_path_to_file> -p <pattern_to_search_for> -u <url> -o <absolute_path_to_output_file>");
    println!("Use the '-h' or '--help' flags for man page.");
    std::process::exit(1);
}

/// This function displays the 'manual' for this program. It explains which arguments are required and which
/// ones are optional. It then exits the program normally(i.e. no error code)
pub fn display_man_page() -> ! {
    println!("SiteScanner - Find patterns in web page HTML.");
    println!("By Tristan Gomez - Winter 2022 - Intro to Rust Programming\n");
    println!("[REQUIRED ARGUMENTS]");
    println!("You must use one or the other flag: -p,--pattern or the -w,--wordlist ");
    println!("-u, --url           Provide a single url to be searched for the provided pattern.");
    println!("-w, --wordlist      Provide an absolute file path for a list of urls to search.");
    println!(
        "-p, --pattern       Provide a RegEx pattern to be searched for on the indicated pages."
    );
    println!("[OPTIONAL ARGUMENTS]");
    println!("-o, --output        Provide an absolute file path to save search results");
    std::process::exit(0);
}
