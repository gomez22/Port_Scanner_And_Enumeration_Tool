# Tristan Gomez - Site Scanner
# Winter 2022

### RustEnum and site scanner video demo!
https://media.pdx.edu/media/t/1_a0opn1a2

## What Was Built
This site scanner tool was a stretch goal for the project, so while functional, it is more of a prototype for a future project. This tool takes a url, and pattern as command-line arguments. The url is to the page being searched and the pattern is the "word" or group of characters being searched for. This pattern could also be a regex. This program also allows the user to provide a list of patterns in a separate file that will be searched for. 

## How the Site Scanner Works
A GET request for the desired page is sent. If a 200 OK response is received then the body of the web page his extracted. The user provided pattern(s) is/are parsed into a RegEx object and the web page body is searched using the RegEx.

## What Didn't Work
My demo video shows this program working for a basic, plain HTML only site; however, when I tried of more modern sites I had difficulty pulling out meaningful information from the body of the GET response. I suspect an encoding issue of some kind, or perhaps I am misreading my output. I would like to continue working on this program and expand its functionality. `RustEnum` is my main deliverable for this term project, so the vast majority of my time was spent developing that. I also had to spend a fair amount of time working out the bugs in RustEnum once the main development stage for it had finished.

## What Lessons Were Learned
I think that I shouldn't have put this as a part of my project. Working on `RustEnum` taught me a lot, but this part of the project didn't teach me anything beyond that. I was able to work out a lot of the Reqwest crate's functionality in `RustEnum` and `RustEnum` was a far more complex program. So in comparison, this was much easier to develop. I think that the challenge ahead will be to provide a smoother user experience and UI. I also think that expanding the functionality of this program will be much more complex than implementing the basic functionality. I also am struggling slightly at this point coming up with more pieces of functionality that could be added.