//! All in One Tool!
//!
//! Wenjing Wu - Winter 2022

use async_std::task;
use clap::{Arg, Command};
use colored::Colorize;
use portscanner::Scanner;
use std::error::Error;
use std::net::{SocketAddr, ToSocketAddrs};
use std::time::Duration;

use futures::{stream, StreamExt};
use indicatif::{ProgressBar, ProgressStyle};
use reqwest::Client;
//use rustenum;
use std::sync::atomic::AtomicUsize;
use std::time::Instant;

//references: https://gitlab.cecs.pdx.edu/gomez22/Port_Scanner_And_Enumeration_Tool/-/blob/main/portscanner/src/main.rs
#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let cli = Command::new("all in one")
        .version("0.1.0")
        .arg_required_else_help(true)
        .arg(
            Arg::new("host")
                .help("The host address to scan")
                .required(true)
                .index(1),
        )
        .arg(
            Arg::new("timeout")
                .help("Connection timeout in milliseconds ")
                .long("timeout")
                .short('t')
                .takes_value(true)
                .default_value("1000"),
        )
        .arg(
            Arg::new("batch_size")
                .help("Number of ports that will be scanned concrrently. Could cause problems if it is too big. Reduce batch_size and try again if scan returns zero open port ")
                .long("batch_size")
                .short('b')
                .takes_value(true)
                .default_value("1000"),
        )
        .arg(
            Arg::new("range_start")
                .help("Start of port range")
                .long("range_start")
                .short('s')
                .requires("range_end")
                .takes_value(true),
        )
        .arg(
            Arg::new("range_end")
                .help("End of port range ")
                .long("range_end")
                .requires("range_start")
                .short('e')
                .takes_value(true),
        )
        .arg(
            Arg::new("full_range")
                .help("Scan port 0-65535 ")
                .long("full_range")
                .short('f'),
        )
        .arg(
            Arg::new("port")
                .help("Scan single port")
                .long("port")
                .short('p')
                .takes_value(true),
        )
		.arg(
            Arg::new("wordlist")
                .help("An absolute file path to a txt file containing website page wordlist for scan. ")
                .long("wordlist")
                .short('w')
                .takes_value(true),
        )
        .get_matches();
    let host = cli.value_of("host").unwrap();

    let timeout = cli
        .value_of("timeout")
        .unwrap()
        .parse::<u64>()
        .unwrap_or(1000);
    let batch_size = cli
        .value_of("batch_size")
        .unwrap()
        .parse::<u16>()
        .unwrap_or(1000);

    let ip_addresses: Vec<SocketAddr> = format!("{}:0", host).to_socket_addrs()?.collect();
    let ps = Scanner::new(Duration::from_millis(timeout));
    let results: Vec<(SocketAddr, String)>;

    //Display title, current configuration and help information of the tool
    let line = format!("{:-<150}", "-").cyan();
    println!("{ }", line);
    println!("{ }", "All in one Tool".bold());
    println!("{ }", format!("{:=<150}", "=").cyan());
    //println!("{ }", line);
    println!("Portscanner - A TCP Port Scanning Tool");
    println!("By Wenjing Wu");
    println!("{ }", line);
    println!(
        "Timeout(milliseconds): { }    Batch size: { }",
        timeout, batch_size
    );
    println!("For help, add flag -h/-help or simply use command 'cargo run'.");
    println!("{ }", line);

    if cli.is_present("port") {
        //single port scan
        let single_port = cli.value_of("port").unwrap().parse::<u16>().unwrap_or(0);
        println!("Scan single port for IP { }", ip_addresses[0].ip());
        let ftr = ps.scan(
            ip_addresses[0].ip(),
            None,
            None,
            batch_size,
            false,
            Some(single_port),
        );
        results = task::block_on(async { ftr.await });
    } else if !cli.is_present("range_start")
        && !cli.is_present("range_end")
        && !cli.is_present("full_range")
    {
        // top 1000 most commonly used ports scan
        println!(
            "Scan top 1000 most commonly used ports for IP { }",
            ip_addresses[0].ip()
        );
        let ftr = ps.scan(ip_addresses[0].ip(), None, None, batch_size, false, None);
        results = task::block_on(async { ftr.await });
    } else if cli.is_present("range_start") && cli.is_present("range_end") {
        //scan by range
        let range_start = cli
            .value_of("range_start")
            .unwrap()
            .parse::<u16>()
            .unwrap_or(0);
        let range_end = cli
            .value_of("range_end")
            .unwrap()
            .parse::<u16>()
            .unwrap_or(65535);
        println!(
            "Scan port from range { } to { } for IP { } ",
            range_start,
            range_end,
            ip_addresses[0].ip()
        );

        let ftr = ps.scan(
            ip_addresses[0].ip(),
            Some(range_start),
            Some(range_end),
            batch_size,
            false,
            None,
        );
        results = task::block_on(async { ftr.await });
    } else {
        //full scan
        println!("Scan all ports (0-65535) for IP { }", ip_addresses[0].ip());
        let ftr = ps.scan(
            ip_addresses[0].ip(),
            Some(0),
            Some(65535),
            batch_size,
            true,
            None,
        );
        results = task::block_on(async { ftr.await });
    }

    //print ip addresses converted from the host name but not scanned
    if ip_addresses.len() > 1 {
        // println!("Other addresses for { } (not scanned):", host);
        let iter: String = ip_addresses
            .iter()
            .skip(1) // skip the scanned ip address
            .enumerate()
            .map(|(i, &addr)| match i {
                0 => (addr.ip().to_string() + " "),
                _ => match i % 6 {
                    0 => (addr.ip().to_string() + "\n"),
                    _ => (addr.ip().to_string() + ", "),
                },
            })
            .collect();
        println!("Other addresses for { } (not scanned):  { }", host, iter);
    }

    println!(" ");

    //print the scanned ip address (first address converted from host name)
    println!("IP:{ }", ip_addresses[0].ip());

    //store http address for rustenum
    let mut http_results = Vec::new();

    //print scan result
    if !results.is_empty() {
        println!(
            "{0: <10} {1: <5} {2: <15}",
            "Open Ports", "State", "Service(reply)"
        );

        for r in results {
            println!("{0: <10} {1: <5} {2: <15}", r.0.port(), "open", r.1);

            //prepare http addresses for rustenum
            if r.1.starts_with("HTTP") {
                //Use hostname:port and ip:port for rustenum will return different reuslt
                //Currently using host, but not sure which one is a better fit.
                let addr = host.to_owned() + ":" + &r.0.port().to_string();
                //let addr =  r.0.ip().to_string() + ":" + &r.0.port().to_string();
                http_results.push(addr);
            }
        }
    } else {
        //suggest to reduce batch size if no open port found
        println!(
        "No open port found.\nCurrent batch size is { }.\nMaybe try again with a smaller batch size using flag '-b [batch size]'", batch_size);
    }

    //if -w/wordlist present, scan with rustenum
    //performance can be improved with concurrency
    if cli.is_present("wordlist") && !http_results.is_empty() {
        let wordlist_path = cli.value_of("wordlist").unwrap();
        for r in http_results {
            rust_enum(wordlist_path.to_string(), &r).await;
        }
    }

    Ok(())
}

///references: https://gitlab.cecs.pdx.edu/gomez22/Port_Scanner_And_Enumeration_Tool/-/blob/main/rustenum/src/main.rs
async fn rust_enum(wordlist_path: String, host: &str) {
    let client = Client::builder()
        // Set the client to never follow redirects.
        .redirect(reqwest::redirect::Policy::none())
        .build()
        .unwrap();

    // Create a new scanner object.
    let mut scanner = rustenum::Scanner::new();

    match scanner.try_add_site(host) {
        // Url successfully parsed, do nothing but continue program execution.
        Ok(_msg) => {}

        // Error when parsing url, we cannot continue program execution.
        // Provide diagnostic message to user, call usage() and exit.
        Err(err) => {
            eprintln!("{}", err);
            usage();
        }
    }

    // Valid site, next step is to check if wordlist is valid.
    // If it is then load its contents into the Scanner object.
    let mut infile = String::new();
    //Store the file name into `infile`
    infile += &wordlist_path;

    scanner.build_wordlist_from_file(infile);
    // Add "404" to the ignore list by default.
    scanner.use_default_ignore_list();

    // Print a nice message to the user.
    let line = format!("{:-<150}", "-").cyan();
    println!("{ }", line);
    println!("{ }", "RustEnum - A webpage enumeration tool".bold());
    println!("By Tristan Gomez");
    println!("{ }", line);

    // This variable is used to keep track of how many webpages have been enumerated.
    // It is an AtomicUsize because it needs to be safely shared between threads.
    let mut words_used = AtomicUsize::new(0);

    // This variable represents the total number of pages needed to be enumerated.
    // It is cast from a usize to a u64 for use in the progress_bar object.
    let wordlist_size = scanner.wordlist.len() as u64;

    // We need to initialize a progress bar with a length that is the number of pages to enumerate through.
    let progress_bar = ProgressBar::new(wordlist_size);

    progress_bar.set_style(
        ProgressStyle::default_bar()
            .template("{spinner:.green} [{elapsed_precise}] [{bar:40.cyan/blue}] {percent}% {msg} ")
            .unwrap()
            .with_key("eta", |state| format!("{:.1}s", state.eta().as_secs_f64()))
            .progress_chars("#>-"),
    );

    // The default number of concurrent/parallel requests that can be sent is 10.
    let thread_count = 10_usize;

    println!("Starting Scan.");
    let now = Instant::now();

    let temp: Vec<(String, u16)> = stream::iter(scanner.wordlist.0.iter())
        // Each word in the stream iterator is then mapped to the block of code that scans a web page.
        .map(|word| {
            // We need to take a reference to the client object so we can use it to send GET requests.
            let client = &client;

            // We need to get the main url/site path from the scanner object.
            let path = scanner.site.clone();

            // Increment the words_used AtomicUsize variable to progress the
            // progress bar.
            let words_used = &mut words_used;
            *words_used.get_mut() += 1;

            // update the progress_bar with the new count of words_used.
            progress_bar.set_position(*words_used.get_mut() as u64);

            async move {
                // construct the full url by concatenating the url_path + "/" + word_from_wordlist
                let path = &path;
                let mut url;
                if path.ends_with('/') {
                    url = path.to_string();
                    url += word;
                } else {
                    url = path.to_string();
                    url += "/";
                    url += word;
                }

                // Send the get request for the url and await a response.
                match client.get(url).send().await {
                    Ok(resp) => process_response(word.to_string(), resp),
                    Err(_) => failed_response(),
                }
            }
        })
        .buffer_unordered(thread_count)
        .collect()
        .await;

    // For every result found, process them.
    // `add_to_found()` ignores any tuple that has
    // a u16 in the scanner's ignore list.
    for result in temp {
        scanner.add_to_found(result);
    }

    // Scan is over, finish and clear the progress bar.
    progress_bar.finish_and_clear();

    println!("Time elapsed: {} seconds", now.elapsed().as_secs());
    scanner.display_found();
}

///references: https://gitlab.cecs.pdx.edu/gomez22/Port_Scanner_And_Enumeration_Tool/-/blob/main/rustenum/src/main.rs
/// This displays some examples on how to use this program. It also points the user to the use the -h/--help flag to
/// get more help with using this program.
fn usage() -> ! {
    println!("./rustenum [optional flags] -w /usr/share/wordlist/dirb/common.txt [optional flags] -u [host]");
    println!("Remember to use the correct HTTP scheme (HTTP/HTTPS) for the --url argument.");
    println!("EXAMPLE: ./rustenum -w /usr/share/wordlists/common.txt -t 4 -u http://10.10.10.10");
    println!("EXAMPLE: ./rustenum -w /usr/share/wordlists/common.txt -u https://10.10.10.10");
    println!("EXAMPLE: ./rustenum -w /usr/share/wordlists/common.txt -u http://example.com");
    println!("EXAMPLE: ./rustenum -w /usr/share/wordlists/common.txt -x .php,html,js -u http://example.com");
    println!("Use the '-h' or '--help' flags for man page.");
    std::process::exit(1);
}

//references: https://gitlab.cecs.pdx.edu/gomez22/Port_Scanner_And_Enumeration_Tool/-/blob/main/rustenum/src/main.rs
// This method processes a reqwest::Response object received from the Client.get() in the scan.
/// This method takes 'ext'(webpage that was requested) and 'resp' as arguments.
pub fn process_response(ext: String, resp: reqwest::Response) -> (String, u16) {
    // If the response is a 301 redirect
    if resp.status().as_u16() == 301 {
        // If the location header was provided in the response object then
        // we need to format the returned object to reflect that. Attempt to
        // get the location header's value.
        if let Ok(location) = resp
            .headers()
            .get(reqwest::header::LOCATION)
            .unwrap()
            .to_str()
        {
            // display the requested page, the page the request
            // should be redirected to, and the http status code.
            (
                ext + "   [REDIRECTED TO: " + location + "]",
                u16::from(resp.status()),
            )

        // Location header doesn't exist. Return (page enumerated, http status code)
        } else {
            (ext, u16::from(resp.status()))
        }

    // If the response was a 302 redirect.
    } else if resp.status().as_u16() == 302 {
        // If the location header was provided in the response object then
        // we need to format the returned object to reflect that. Attempt to
        // get the location header's value.
        if let Ok(location) = resp
            .headers()
            .get(reqwest::header::LOCATION)
            .unwrap()
            .to_str()
        {
            // display the requested page, the page the request
            // should be redirected to, and the http status code.
            (ext + "   [" + location + "]", u16::from(resp.status()))

        // Location header doesn't exist. Return (page enumerated, http status code)
        } else {
            (ext, u16::from(resp.status()))
        }
    } else {
        // Normal case. Return the page requested and its http response.
        (ext, u16::from(resp.status()))
    }
}

/// Generic failed response. Returns a 404 status code and a placeholder value of "failed".
/// Since the status code is 404, it will be ignored by the scanner in this program.
pub fn failed_response() -> (String, u16) {
    ("failed".to_string(), 404u16)
}
