# All-in-One Tool
Wenjing Wu

## What Was Built
All-in-One Tool should be a supportive tool that allows users to chain multiple tools with one command.
Current implementaion includes all [portscanner](../portscanner) functionalities, as well as basic [rustenum](../rustenum) functionalities provided by required flags (`-w <path_to_wordlist>` and `-u <base_url>`).
We believe future expansion can be build upon this prototype implementation. 

## How it works
First, the tool scans tcp ports on a given host/ip and return a list of open ports associated with HTTP services.
Second, it scans files/directories with the same host/ip for each http ports.
Rustenum only runs when `-w` flag is present. Otherwise, all-in-one Tool behaves the same as portscanner. 

Usage and command-line flags are listed below:

```shell
all in one 0.1.0

USAGE:
    allinone [OPTIONS] <host>

ARGS:
    <host>    The host address to scan

OPTIONS:
    -b, --batch_size <batch_size>      Number of ports that will be scanned concrrently. Could cause
                                       problems if it is too big. Reduce batch_size and try again if
                                       scan returns zero open port  [default: 1000]
    -e, --range_end <range_end>        End of port range
    -f, --full_range                   Scan port 0-65535
    -h, --help                         Print help information
    -p, --port <port>                  Scan single port
    -s, --range_start <range_start>    Start of port range
    -t, --timeout <timeout>            Connection timeout in milliseconds  [default: 1000]
    -V, --version                      Print version information
    -w, --wordlist <wordlist>          An absolute file path to a txt file containing website page
                                       wordlist for scan.
```
### How to run:
* To get the help information above: `cargo run` or `Cargo run -- -h/--help`
* Example: `cargo run -- kerkour.com -s 80 -e 500 -w ./common.txt`. 

## What Didn't Work
My biggest struggle was usecase generalization since it varies depending on each CTF competition. 
For example, running rustenum for IP:8080 vs hostname:8080 will return different sets of results. 
The code is currently set up to choose the later, but I am not sure if it's the better one. 

## What Lessons Were Learned
While working on this tool, I learnt that modularity was very important. 
Even though I refactored my code a couple times, I can see the maintainability is going to become a problem soon. 


## Refernces
* https://stackoverflow.com/questions/51044467/how-can-i-perform-parallel-asynchronous-http-get-requests-with-reqwest
* https://kerkour.com/rust-worker-pool/
* https://github.com/console-rs/indicatif
* https://github.com/RustScan/RustScan/tree/master/src
* https://kerkour.com/rust-fast-port-scanner
* [portscanner/src/main.rs](../portscanner/src/main.rs)
* [rustenum/src/main.rs](../rustenum/src/main.rs)

